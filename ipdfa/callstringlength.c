int id2(int i)
{
  return i;
}

int id1(int i)
{
  return id2(i);
}

int add(int a, int b)
{
  return id1(a) + id1(b);
}

/* The constant value of z can only be calculated with callstringlength >=2 */
int main()
{
  int x, y, z;
  x = 10;
  y = 20;
  z = add(x, y);
  return z;

}
